CREATE DATABASE dbcampionato;

CREATE TABLE dbcampionato.campionato (
	id int PRIMARY KEY AUTO_INCREMENT,
    nome varchar(30),
    anno int
    );

CREATE TABLE dbcampionato.arbitro (
	cf varchar(16) PRIMARY KEY AUTO_INCREMENT,
    nome varchar(20),
    cognome varchar(30)
    );
    
CREATE TABLE dbcampionato.luogo (
	id int PRIMARY KEY,
    nome varchar(30),
    città varchar(30)
    );
    
CREATE TABLE dbcampionato.giocatore (
	id int PRIMARY KEY AUTO_INCREMENT,
    nome varchar(20),
    cognome varchar(30),
    data_nascita date,
    luogo_nascita varchar(30)
    );
    
CREATE TABLE dbcampionato.squadra (
	id int PRIMARY KEY AUTO_INCREMENT,
    nome varchar(30),
    città varchar(30)
    );

CREATE TABLE dbcampionato.partita (
	id int PRIMARY KEY AUTO_INCREMENT,
    dataPartita date,
    id_campionato int,
    id_luogo int,
    id_squadra1 int,
    id_squadra2 int,
    punti_squadra1 int,
    punti_squadra2 int,
    FOREIGN KEY (id_campionato) REFERENCES dbcampionato.campionato(id),
    FOREIGN KEY (id_luogo) REFERENCES dbcampionato.luogo(id),
    FOREIGN KEY (id_squadra1) REFERENCES dbcampionato.campionato(id),
    FOREIGN KEY (id_squadra2) REFERENCES dbcampionato.campionato(id)
    );
    
CREATE TABLE dbcampionato.direzione (
	cf_arbitro varchar(16),
    id_partita int,
    FOREIGN KEY (cf_arbitro) REFERENCES dbcampionato.arbitro(cf),
    FOREIGN KEY (id_partita) REFERENCES dbcampionato.partita(id),
    CONSTRAINT pk_direzione PRIMARY KEY (cf_arbitro, id_partita)
    );
    
CREATE TABLE dbcampionato.formazione (
	id_giocatore int,
    id_squadra int,
    anno int,
    numero int,
    FOREIGN KEY (id_giocatore) REFERENCES dbcampionato.giocatore(id),
    FOREIGN KEY (id_squadra) REFERENCES dbcampionato.squadra(id),
    CONSTRAINT pk_formazuibe PRIMARY KEY (id_giocatore, id_squadra, anno)
    );
    
CREATE TABLE dbcampionato.segna (
	id_giocatore int,
    id_partita int,
    minuto int,
    FOREIGN KEY (id_giocatore) REFERENCES dbcampionato.giocatore(id),
    FOREIGN KEY (id_partita) REFERENCES dbcampionato.partita(id),
    CONSTRAINT pk_segna PRIMARY KEY (id_giocatore, id_partita, minuto)
    );
    
ALTER TABLE dbcampionato.luogo
ADD COLUMN regione varchar(30),
ADD COLUMN provincia varchar(30);

ALTER TABLE dbcampionato.squadra
ADD COLUMN colore_squadra varchar(15);    

ALTER TABLE dbcampionato.luogo job
MODIFY COLUMN nome varchar(255);

INSERT INTO dbcampionato.campionato (nome, anno
) VALUES (
        "Serie A",
        2014
        );

SELECT * FROM dbcampionato.campionato;

UPDATE dbcampionato.campionato SET nome = "Premier League"
WHERE id = 2;

UPDATE dbcampionato.campionato SET anno = anno + 1
WHERE nome = "Serie A" and id = 3;



    