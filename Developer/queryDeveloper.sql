-- Query 2

-- Aggiungo innanzitutto dei coordinatori, tutti campi delle insert saranno generati per semlicità grazie all'aiuto di copilot

INSERT INTO coordinatore (codice_fiscale, nome, cognome, telefono, mail, numero_progetti)
VALUES
    ('MCHLEER31M18F708', 'Michael', 'Lee', 3889764571, 'michael.lee@virgilio.com', 6),
    ('PPLPRIN78T67FIOL', 'Paolo', 'Primo', 3881234571, 'paolo.primo@gmail.com', 4),
    ('ANNGLLM98H08F569', 'Anna', 'Gialli', 3234764571, 'anna.gialli@alice.com', 2),
    ('LCCCRLM98J09H765', 'Luca', 'Corallo', 3667894317, 'luca.corallo@gmail.com', 1),
    ('CLALECM16L01H876', 'Charles', 'Leclerc', 3998764512, 'charles.lece@ferrari.com', 3);
    
-- Aggiungo 3 progetti al database

INSERT INTO progetto (id, nome, tipologia, codice_fiscale_coordinatore) 
VALUES
	(1, "Starship", "Programmazione", 'PPLPRIN78T67FIOL'),
    (2, "UniBanking", "Programmazione", 'ANNGLLM98H08F569'),
    (3, "F1 logo", "Grafica", 'CLALECM16L01H876');
    
-- Aggiungo i task relativi ai 3 progetti

INSERT INTO task (descrizione, durata_giorni, stato, id_progetto)
VALUES 
	("Calcolo traiettoria missile", 180, "Chiuso", 1),
    ("Progettazione software abitacolo", 180, "Chiuso", 1),
    ("Modellazione database banca", 270, "Chiuso", 2),
    ("Progettazione UIX", 180, "Aperto", 2),
    ("Design concettuale", 60, "Aperto", 3);
		
-- Query 1

select distinct progetto.nome as Progetti_Attivi from progetto join task
on progetto.id = task.id_progetto
where task.stato = "Aperto";

-- Query 3

-- Inserisco i collaboratori

INSERT INTO collaboratore (codice_fiscale, nome, cognome, telefono, mail, id_task)
VALUES
    ('PPRPPRM31M98H908', 'Piero', 'Pieri', 3234764571, 'piero.pieri@email.com', 4),
    ('MRALNIM34M56H98I', 'Maria', 'Rossi', 3401234567, 'maria.rossi@email.com', 4),
    ('GLIBNCH18U78POL3', 'Giulia', 'Bianchi', 3266543210, 'giulia.bianchi@email.com', 4),
    ('LCUVRDM98I98P909', 'Luca', 'Verdi', 3454567890, 'luca.verdi@email.com', 5),
    ('MRCGLLM98L09I892', 'Marco', 'Gialli', 3106543210, 'marco.gialli@email.com', 5);
    
-- Inserisco i file

INSERT INTO file (nome, tipo, id_task) 
VALUES
	("traiettoria", "drw", 1),
    ("comandi_abitacolo", "txt", 2),
    ("requisiti_database", "txt", 3),
    ("modelloEr", "drw", 3),
    ("uix_model", "png", 4),
    ("index", "html", 4),
    ("server", "java", 4),
    ("logo", "jpg", 5);
    
-- Inserisco le modifiche ai file

INSERT INTO modifica_file
VALUES 
	('PPRPPRM31M98H908', 1, '2022-12-12'),
    ('GLIBNCH18U78POL3', 2, '2022-12-15'),
    ('PPRPPRM31M98H908', 2, '2022-12-19'),
    ('MRALNIM34M56H98I', 3, '2023-01-15'),
    ('PPRPPRM31M98H908', 3, '2023-01-20'),
    ('MRALNIM34M56H98I', 4, '2023-02-12'),
    ('GLIBNCH18U78POL3', 5, '2023-02-16'),
    ('MRALNIM34M56H98I', 5, '2023-02-18'),
    ('PPRPPRM31M98H908', 6, '2023-02-28'),
    ('MRALNIM34M56H98I', 7, '2023-03-15'),
    ('LCUVRDM98I98P909', 8, '2023-03-05'),
    ('MRCGLLM98L09I892', 8, '2023-03-19');
    
DELETE FROM modifica_file where id_file = 5;
    
-- Svolgo la query

select progetto.nome, count(modifica_file.data_di_modifica) from progetto join task
on progetto.id = task.id_progetto join file
on task.id = file.id_task join modifica_file
on file.id = modifica_file.id_file
where task.stato = "Aperto"
group by progetto.id
limit 1;

-- Query 4

select progetto.nome, data_di_modifica from progetto join task
on progetto.id = task.id_progetto join file
on task.id = file.id_task join modifica_file as q1
on file.id = q1.id_file
where task.stato = "Aperto"
and q1.data_di_modifica > any (select data_di_modifica from modifica_file as q2
								where q1.id_file = q2.id_file)
and DATE_SUB(CURDATE(), INTERVAL 1 year) > q1.data_di_modifica;

-- Query 5

INSERT INTO skill 
VALUES 
	("Programmazione Java", "Programmazione"),
    ("Programmazione C", "Programmazione"),
    ("React", "Programmazione"),
    ("Design UIX", "Grafica"),
    ("3D Modeling", "Grafica");
    
    
INSERT INTO possiede_skill_collaboratore
VALUES 
	('PPRPPRM31M98H908', "Programmazione Java", 8),
    ('PPRPPRM31M98H908', "Programmazione C", 7),
    ('PPRPPRM31M98H908', "Database", 7),
    ('PPRPPRM31M98H908', "React", 6),
    ('MRALNIM34M56H98I', "Database", 9),
    ('MRALNIM34M56H98I', "React", 7),
    ('GLIBNCH18U78POL3', "React", 8),
    ('GLIBNCH18U78POL3', "Programmazione Java", 7),
    ('LCUVRDM98I98P909', "Design UIX", 8),
    ('LCUVRDM98I98P909', "3d Modeling", 7),
	('MRCGLLM98L09I892', "Design UIX", 7),
    ('MRCGLLM98L09I892', "3d Modeling", 9);
	
DELIMITER //

CREATE PROCEDURE cercaSviluppatorePerCapacita(IN prima_skill varchar(40), IN seconda_skill varchar(40))
		BEGIN
			SELECT distinct collaboratore.nome, collaboratore.cognome, skill.nome from collaboratore join possiede_skill_collaboratore
			on collaboratore.codice_fiscale = possiede_skill_collaboratore.codice_fiscale_collaboratore join skill
            on skill.nome = possiede_skill_collaboratore.nome_skill
            where skill.nome = prima_skill or skill.nome = seconda_skill;
		END//
        
DELIMITER ;

call cercaSviluppatorePerCapacita("Programmazione Java", "React");
			
-- Query 6

DELIMITER //

CREATE PROCEDURE cercaProgetto(IN nome_progetto varchar(30))
		BEGIN
			select progetto.nome from progetto
            where progetto.nome = nome_progetto;
		END//
        
DELIMITER ;

call cercaProgetto("Starship");

-- Query 7 

DELIMITER //

CREATE PROCEDURE inserisciSviluppatore(IN cf_collaboratore varchar(16), IN nome_collaboratore varchar(20), IN cognome_collaboratore varchar(20), IN telefono_collaboratore numeric(10), IN mail_collaboratore varchar(40), IN task_collaboratore int)
		BEGIN
			insert into collaboratore
            values (cf_collaboratore, nome_collaboratore, cognome_collaboratore, telefono_collaboratore, mail_collaboratore, task_collaboratore);
		END //
        
DELIMITER ;

call inserisciSviluppatore('LCURVNM18N98T763', 'Luca', 'Ravenna', 3456789871, 'luca.ravenna@example.com', 5);

select * from collaboratore;

-- Query 8

DELIMITER //

CREATE PROCEDURE aggiornaSkillCollaboratore(IN cf_collaboratore varchar(16), IN nome_skill varchar(30), IN livello_skill int)
		BEGIN
			insert into possiede_skill_collaboratore
            values (cf_collaboratore, nome_skill, livello_skill);
		END //
        
DELIMITER ;


call aggiornaSkillCollaboratore('GLIBNCH18U78POL3', 'Programmazione C', 6);

select collaboratore.nome, skill.nome, possiede_skill_collaboratore.livello from collaboratore join possiede_skill_collaboratore
on collaboratore.codice_fiscale = possiede_skill_collaboratore.codice_fiscale_collaboratore join skill
on skill.nome = possiede_skill_collaboratore.nome_skill
where collaboratore.codice_fiscale = 'GLIBNCH18U78POL3';

-- Query 9

DELIMITER //

CREATE PROCEDURE inserisciValutazione(IN cf_collaboratore varchar(16), cf_coordinatore varchar(16),IN task_svolto int, IN voto_task int)
		BEGIN
			insert into valutazione
            values (cf_collaboratore, cf_coordinatore, task_svolto, voto_task);
		END //
        
DELIMITER ;

call inserisciValutazione('PPRPPRM31M98H908', 'PPLPRIN78T67FIOL', 1, 4);

select * from valutazione;

-- Query10

DELIMITER //

CREATE PROCEDURE generaCurriculum(IN cf_collaboratore varchar(16))
		BEGIN
			select collaboratore.nome, collaboratore.cognome, progetto.nome, task.descrizione, valutazione.voto from collaboratore join modifica_file
			on collaboratore.codice_fiscale = modifica_file.codice_fiscale_collaboratore join file 
			on modifica_file.id_file = file.id join task
			on file.id_task = task.id join progetto 
			on task.id_progetto = progetto.id left join valutazione
			on collaboratore.codice_fiscale = valutazione.codice_fiscale_collaboratore and valutazione.task_id = task.id
            where collaboratore.codice_fiscale = cf_collaboratore;
		END //
        
DELIMITER ;

call generaCurriculum('PPRPPRM31M98H908');

-- Query 11

DELIMITER //

CREATE PROCEDURE cambiaDurata(IN durata int, IN id_task int)
		BEGIN
			UPDATE task SET task.durata_giorni = durata
            where task.id = id_task;
		END //
        
DELIMITER ;

call cambiaDurata(300, 5);

select * from task;

-- Query 12

DELIMITER //

CREATE PROCEDURE eliminaSviluppatore(IN cf_sviluppatore varchar(16))
		BEGIN
			UPDATE collaboratore SET collaboratore.id_task = NULL
            where collaboratore.codice_fiscale = cf_sviluppatore;
		END //
        
DELIMITER ;

call eliminaSviluppatore('MRALNIM34M56H98I');

select * from collaboratore;

-- Query 13

DELIMITER //

CREATE PROCEDURE cercaVersione(IN data_versione date, IN file_cercato int, OUT data_modificata date)
		BEGIN
			select modifica_file.data_di_modifica into data_modificata from modifica_file
            where modifica_file.data_di_modifica = data_versione;
		END //
        
DELIMITER ;

DELIMITER //

CREATE PROCEDURE aggiornaVersione(IN data_versione date, in aggiornamento_file int)
		BEGIN
			declare data_corretta date default null;
            call cercaVersione(data_versione, aggiornamento_file, data_corretta);
			update file set versione_data = data_corretta
			where file.id = aggiornamento_file;
		END //
        
DELIMITER ;

call aggiornaVersione('2022-12-15', 2);

select * from file;

-- Query 14

DELIMITER //

CREATE PROCEDURE generaStorico(in storia_file int)
		BEGIN
			select collaboratore.nome, collaboratore.cognome, modifica_file.data_di_modifica from collaboratore join modifica_file
            on collaboratore.codice_fiscale = modifica_file.codice_fiscale_collaboratore
            where modifica_file.id_file = storia_file;
		END //
        
DELIMITER ;

call generaStorico(2);

        
