create database dbdevelopers;

use dbdevelopers;

create table coordinatore(
	codice_fiscale varchar(16) primary key,
    nome varchar(20),
    cognome varchar(20),
    telefono numeric(10),
    mail varchar(40),
    numero_progetti int);
    
create table collaboratore(
	codice_fiscale varchar(16) primary key,
    nome varchar(20),
    cognome varchar(20),
    telefono numeric(10),
    mail varchar(40),
    id_task int);

create table curriculum_collaboratore(
	codice_fiscale_collaboratore varchar(16) primary key,
    formato varchar(3),
    constraint fk_curricul_collaboratore foreign key (codice_fiscale_collaboratore) references collaboratore(codice_fiscale),
    check(formato in ("TXT","PDF"))
    );
    
create table curriculum_coordinatore(
	codice_fiscale_coordinatore varchar(16) primary key,
    formato varchar(3),
    constraint fk_curricul_coordinatore foreign key (codice_fiscale_coordinatore) references coordinatore(codice_fiscale),
    check(formato in ("TXT","PDF"))
    );
    
create table file (
	id int primary key auto_increment,
    nome varchar(30),
    tipo varchar(3),
    id_task int);

ALTER TABLE file
MODIFY COLUMN tipo varchar(10);

alter TABLE FILE
add column versione_data date;


create table modifica_file (
	codice_fiscale_collaboratore varchar(16),
    id_file int,
    data_di_modifica date,
    constraint fk_modifica_collaboratore foreign key (codice_fiscale_collaboratore) references collaboratore(codice_fiscale),
    constraint fk_modifica_file foreign key (id_file) references file(id),
    constraint pk_mofifica_file primary key (codice_fiscale_collaboratore, id_file, data_di_modifica));
    
    

create table task (
	id int primary key auto_increment,
    descrizione varchar(50),
    durata_giorni int,
    stato varchar(6),
    id_progetto int,
    check(stato in ("Aperto", "Chiuso"))
    );
    
alter table collaboratore
add constraint fk_collaboratore foreign key (id_task) references task(id);

alter table file
add constraint fk_file foreign key (id_task) references task(id);

create table progetto (
	id int primary key auto_increment,
    nome varchar(30),
    tipologia varchar(30),
    codice_fiscale_coordinatore varchar(16),
    constraint fk_progetto foreign key (codice_fiscale_coordinatore) references coordinatore(codice_fiscale)
    );
    
alter table task
add constraint fk_task foreign key (id_progetto) references progetto(id);

create table messaggio_coordinatore (
	id int primary key auto_increment,
    tipo varchar(40),
    visibilità varchar(20),
    codice_fiscale_coordinatore varchar(16),
    constraint fk_messaggio_coordinatore foreign key (codice_fiscale_coordinatore) references coordinatore(codice_fiscale)
    );
    
create table messaggio_collaboratore (
	id int primary key auto_increment,
    tipo varchar(40),
    visibilità varchar(20),
    codice_fiscale_collaboratore varchar(16),
    constraint fk_messaggio_collaboratore foreign key (codice_fiscale_collaboratore) references collaboratore(codice_fiscale)
    );
    
create table skill (
	nome varchar(40) primary key,
    tipologia varchar(30)
    );
    
create table richiede_skill (
	id_task int,
    nome_skill varchar(40),
    livello int,
    check(livello >= 0 and livello <= 10),
    constraint fk_richiede_task foreign key (id_task) references task(id),
    constraint fk_richiede_skill foreign key (nome_skill) references skill(nome),
    constraint pk_richiede_skill primary key (id_task, nome_skill)
    );

create table possiede_skill_coordinatore(
	codice_fiscale_coordinatore varchar(16),
    nome_skill varchar(40),
    livello int,
    check(livello >= 0 and livello <= 10),
    constraint fk_skill_coordinatore foreign key (codice_fiscale_coordinatore) references coordinatore(codice_fiscale),
    constraint fk_possiede_skill_coordinatore foreign key (nome_skill) references skill(nome),
    constraint pk_possiede_skill_coordinatore primary key (codice_fiscale_coordinatore, nome_skill)
    );
    
create table possiede_skill_collaboratore(
	codice_fiscale_collaboratore varchar(16),
    nome_skill varchar(40),
    livello int,
    check(livello >= 0 and livello <= 10),
    constraint fk_skill_collaboratore foreign key (codice_fiscale_collaboratore) references collaboratore(codice_fiscale),
    constraint fk_possiede_skill_collaboratore foreign key (nome_skill) references skill(nome),
    constraint pk_possiede_skill_collaboratore primary key (codice_fiscale_collaboratore, nome_skill)
    );
    
create table valutazione (
	codice_fiscale_collaboratore varchar(16),
    codice_fiscale_coordinatore varchar(16),
    task_id int,
    voto int,
    check(voto >= 0 and voto <= 5),
    constraint fk_valutazione_collaboratore foreign key (codice_fiscale_collaboratore) references collaboratore(codice_fiscale),
    constraint fk_valutazione_coordinatore foreign key (codice_fiscale_coordinatore) references coordinatore(codice_fiscale),
    constraint fk_valutazione_task foreign key (task_id) references task(id),
    constraint pk_valutazione primary key (codice_fiscale_collaboratore, codice_fiscale_coordinatore, task_id)
    );
    
