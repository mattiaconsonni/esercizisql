create database dbshop;

create table dbshop.venditore (
	id int primary key auto_increment,
    nome varchar(20),
    mail varchar(30),
    telefono numeric(10),
    indirizzo varchar(30),
    stato varchar(20),
    città varchar(20),
    cap varchar(5),
    dataDiRegistrazione date);
    
create table dbshop.prodotto (
	id int primary key auto_increment,
    nome varchar(20),
    descrizione varchar(50),
    prezzo float,
    quantitàDisponibile int,
    categoria varchar(30),
    valutazioneMedia float); 
    

create table dbshop.acquirente (
	id int primary key auto_increment,
    nome varchar(20),
    mail varchar(30),
    telefono numeric(10),
    indirizzo varchar(30),
    stato varchar(20),
    città varchar(20),
    cap varchar(5),
    dataDiRegistrazione date);
    
create table dbshop.ordine (
	id int primary key auto_increment,
    id_acquirente int,
    totale float,
    dataOrdine date,
    stato varchar(20),
    constraint fk_ordine foreign key (id_acquirente) references dbshop.acquirente(id)
    );
    
create table dettaglio_ordine (
	id int primary key auto_increment,
    id_ordine int,
    id_prodotto int,
    quantità int,
    prezzoUnitario float,
    totaleRiga int,
    constraint fk_dettaglio_ordine_ordine foreign key (id_ordine) references dbshop.ordine(id),
    constraint fk_dettaglio_ordine_prodotto foreign key (id_prodotto) references dbshop.prodotto(id)
    );
    
create table dbshop.recensione_prodotto (
	id int primary key auto_increment,
    id_prodotto int,
    id_acquirente int,
    valutazione float,
    testoRecensione varchar(50),
    dataRecensione date,
    constraint fk_recensione_prodotto_prodotto foreign key (id_prodotto) references dbshop.prodotto(id),
    constraint fk_recensione_prodotto_acquirente foreign key (id_acquirente) references dbshop.acquirente(id)
    );
    
create table dbshop.recensione_venditore (
	id int primary key auto_increment,
    id_venditore int,
    id_acquirente int,
    valutazione float,
    testoRecensione varchar(50),
    dataRecensione date,
    constraint fk_recensione_venditore_venditore foreign key (id_venditore) references dbshop.venditore(id),
    constraint fk_recensione_venditore_acquirente foreign key (id_acquirente) references dbshop.acquirente(id)
    );
    
-- Query 1
    
INSERT INTO dbshop.venditore (nome, mail, telefono, indirizzo, stato, città, cap, dataDiRegistrazione)
VALUES (
		"Amazon", "info@amazon.com", 3661898971, "Via Gianni Rodari 5", "Italia", "Milano", "20187", "2005-05-12"),
        ("Armani", "info@armani.it", 3664568971, "Via Albero Manna 1", "Italia", "Milano", "20234","2015-12-07"),
        ("Ferrari", "info@ferrari.com", 3387653478, "Via Maranello 8", "Italia", "Modena", "40175", "2000-02-01"),
        ("Guess", "info@guess.com", 3451548971, "Via Maria Callas", "Italia", "Roma", "25679", "2012-05-23"),
        ("Ikea", "info@ikea.com", 3401236781, "Via Carlo Magno", "Italia", "Milano", "20123", "2023-11-08"
        );
        
INSERT INTO dbshop.prodotto (nome, descrizione, prezzo, quantitàDisponibile, categoria, valutazioneMedia)
VALUES 	('T-shirt', 'T-shirt di cotone', 19.99, 100, 'Vestiario', 4.5),
		('Sneakers', 'Sneakers comode', 79.99, 50, 'Vestiario', 4.2),
        ('Zaino', 'Zaino impermeabile', 39.99, 30, 'Accessorio', 4.0),
        ('Smartphone', 'Smartphone di nuova generazione', 699.99, 10, 'Elettronica', 4.8),
        ('Macchina del caffè', 'Macchinetta del caffè automatica', 89.99, 20, 'Elettronica', 4.6),
        ('Headphones', 'Headphones con cancellazione del rumore', 129.99, 15, 'Elettronica', 4.4),
        ('Orologio', 'Orologio elegante', 149.99, 25, 'Accessorio', 4.3),
        ('Laptop', 'Laptop', 1299.99, 5, 'Elettronica', 4.9),
        ('Occhiali da Sole', 'Occhiali da sole protettivi', 29.99, 40, 'Accessorio', 4.1),
        ('PS5', 'Console PS5 da gioco', 49.99, 60, 'Elettronica', 4.7);
        
INSERT INTO dbshop.acquirente (nome, mail, telefono, indirizzo, stato, città, cap, dataDiRegistrazione)
VALUES 	('Mario Rossi', 'mario.rossi@email.it', 3456789123, 'Via Roma 10', 'Italia', 'Milano', '20121', '2024-02-12'),
		('Laura Bianchi', 'laura.bianchi@email.it', 3335557890, 'Corso Vittorio Emanuele 15', 'Italia', 'Roma', '00186', '2024-01-05'),
        ('Giovanni Verdi', 'giovanni.verdi@email.it', 3471234567, 'Via Dante 5', 'Italia', 'Firenze', '50122', '2024-01-22'),
		('Anna Esposito', 'anna.esposito@email.it', 3409876543, 'Piazza del Popolo 20', 'Italia', 'Napoli', '80121', '2024-03-20'),
        ('Luigi Ferrari', 'luigi.ferrari@email.it', 3495678901, 'Via Garibaldi 30', 'Italia', 'Torino', '10122', '2022-02-11'),
        ('Elena Russo', 'elena.russo@email.it', 3467890123, 'Corso Umberto I 25', 'Italia', 'Palermo', '90133', '2023-05-20'),
        ('Paolo Bianchi', 'paolo.bianchi@email.it', 3331234567, 'Via Garibaldi 15', 'Italia', 'Genova', '16121', '2024-04-05'),
        ('Francesca Romano', 'francesca.romano@email.it', 3409876543, 'Via Dante 25', 'Italia', 'Bologna', '40121', '2024-02-20'),
        ('Marco De Luca', 'marco.deluca@email.it', 3475678901, 'Corso Vittorio Emanuele 10', 'Italia', 'Venezia', '30121', '2024-04-07'),
        ('Silvia Russo', 'silvia.russo@email.it', 3461234567, 'Piazza Navona 5', 'Italia', 'Roma', '00186', '2022-12-20'),
        ('Antonio Ferrari', 'antonio.ferrari@email.it', 3497890123, 'Via Montenapoleone 20', 'Italia', 'Milano', '20121', '2021-12-19'),
        ('Roberto Esposito', 'roberto.esposito@email.it', 3456789012, 'Via Milano 5', 'Italia', 'Bari', '70121', '2024-01-18'),
        ('Maria Conti', 'maria.conti@email.it', 3334567890, 'Corso Umberto I 30', 'Italia', 'Catania', '95121', '2019-04-20'),
        ('Giuseppe Russo', 'giuseppe.russo@email.it', 3467890123, 'Via Garibaldi 25', 'Italia', 'Palermo', '90133', '2022-10-21'),
        ('Luisa Martini', 'luisa.martini@email.it', 3456789012, 'Via Garibaldi 30', 'Italia', 'Firenze', '50122', '2024-04-17');

-- Query 2
        
update dbshop.venditore set mail = "amazon_info@amazon.com"
where id = 1 and nome = "Amazon";
        
select * from dbshop.venditore;

update dbshop.prodotto set quantitàDisponibile = 150
where id = 12 and nome = "Sneakers";
        
select * from dbshop.prodotto;        

update dbshop.acquirente set telefono = 3409875641
where id = 10 and nome = "Silvia Russo";
        
select * from dbshop.acquirente;
        
-- Query 3

delete from dbshop.prodotto where id = 13 and nome = "Zaino";

select * from dbshop.prodotto;  

delete from dbshop.acquirente where id = 2 and nome = 'Laura Bianchi';

select * from dbshop.acquirente;

-- Query 4

select nome, valutazioneMedia from dbshop.prodotto
where valutazioneMedia >= 4.0;

-- Query 5

insert into dbshop.recensione_venditore (id_venditore, id_acquirente, valutazione, testoRecensione, dataRecensione)
values	(1, 1, 4.5, "Ottimo venditore", "2023-01-05"),
		(2, 5, 3.5 , "Consigliato", "2022-10-04"),
        (4, 8, 2.5 , "Sconsigliato", "2022-10-04"),
        (5, 12, 5.0 , "Perfetto", "2024-10-12"),
        (2, 4, 1.9 , "Una truffa", "2022-02-19");
        
select * from dbshop.recensione_venditore;

select  id_venditore, avg(valutazione) as valutazioneMedia from dbshop.recensione_venditore 
group by id_venditore
order by valutazioneMedia desc;


-- Query 

insert into dbshop.ordine (id_acquirente, totale, dataOrdine, stato)
values 	(1, 450, "2024-09-19", "Consegnato"),
		(5, 500, "2024-09-19", "Consegnato"),
        (1, 10, "2024-09-19", "Consegnato"),
        (3, 120, "2024-09-19", "In consegna"),
        (7, 40, "2024-09-19", "Consegnato"),
        (7, 450, "2024-09-19", "Spedito");

select id_acquirente, count(id_acquirente) as numero_ordini from dbshop.ordine
group by id_acquirente
order by numero_ordini desc;

-- Query 6

select nome, categoria, prezzo from dbshop.prodotto
where categoria = "Elettronica" and prezzo < 500;

-- Query 7

select nome from dbshop.acquirente
where id = 7;

-- Query 8

insert into dbshop.recensione_venditore (id_venditore, id_acquirente, valutazione, testoRecensione, dataRecensione)
values	(1, 1, 4.5, "Ottimo venditore", "2023-01-05");

select * from dbshop.recensione_venditore;

select id_venditore, count(id_venditore) as numeroVendite from dbshop.recensione_venditore
group by id_venditore
order by numeroVendite desc
limit 1;

-- Query 9

insert into dbshop.recensione_prodotto (id_prodotto, id_acquirente, valutazione, testoRecensione, dataRecensione)
values	(14, 1, 4.5, "Ottimo venditore", "2023-01-05"),
		(14, 5, 3.5 , "Consigliato", "2022-10-04"),
        (18, 8, 2.5 , "Sconsigliato", "2022-10-04"),
        (11, 12, 5.0 , "Perfetto", "2024-10-12"),
        (19, 4, 1.9 , "Una truffa", "2022-02-19");
        
select id_prodotto, count(id_prodotto) as numeroProdottiVenduti from dbshop.recensione_prodotto
group by id_prodotto
order by numeroProdottiVenduti desc;
        

-- Query 10


		
        

        
        
        
		

        
    

    