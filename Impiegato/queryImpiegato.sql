-- Esercizio1

SELECT FIRST_NAME, LAST_NAME FROM ql.employees;

SELECT DEPARTMENT_ID FROM ql.departments;

-- Esercizio2

SELECT SUBSTRING(FIRST_NAME, 1, 3) AS FIRST_NAME_CHARACTERS FROM ql.employees;

-- Esercizio3

SELECT FIRST_NAME, LAST_NAME, SALARY FROM ql.employees
WHERE SALARY NOT BETWEEN 10000 AND 15000;

-- Esercizio4

SELECT FIRST_NAME, LAST_NAME,DEPARTMENT_ID FROM ql.employees
WHERE DEPARTMENT_ID = 30 OR DEPARTMENT_ID = 100;

-- Esercizio5

SELECT FIRST_NAME, LAST_NAME, HIRE_DATE FROM ql.employees
WHERE HIRE_DATE >= "1987-01-01" AND HIRE_DATE <= "1987-12-31";

-- Esercizio6

SELECT FIRST_NAME, LAST_NAME FROM ql.employees
WHERE LENGTH(FIRST_NAME) = 6;

-- Esercizio7

SELECT * FROM ql.employees
WHERE LAST_NAME = "Jones"
OR LAST_NAME = "Blake"
OR LAST_NAME = "Scott"
OR LAST_NAME = "King"
OR LAST_NAME = "Ford";

-- Esercizio8

SELECT * FROM ql.employees
ORDER BY FIRST_NAME DESC;

SELECT FIRST_NAME, LAST_NAME, SALARY, SALARY - (SALARY * 0.22) AS SALARY_TOTAL  FROM ql.employees;

-- Esercizio9

SELECT * FROM ql.departments;

SELECT FIRST_NAME, HIRE_DATE, DATE_ADD(HIRE_DATE, INTERVAL 90 DAY) AS INTERWIEW_DATE FROM ql.employees
WHERE DEPARTMENT_ID = 10;

-- Esercizio10

SELECT * FROM ql.employees
WHERE REGEXP_LIKE(FIRST_NAME, '[[:digit:]]');

SELECT * FROM ql.employees
WHERE FIRST_NAME LIKE '%0%'
    OR FIRST_NAME LIKE '%1%'
    OR FIRST_NAME LIKE '%2%'
    OR FIRST_NAME LIKE '%3%'
    OR FIRST_NAME LIKE '%4%'
    OR FIRST_NAME LIKE '%5%'
    OR FIRST_NAME LIKE '%6%'
    OR FIRST_NAME LIKE '%7%'
    OR FIRST_NAME LIKE '%8%'
    OR FIRST_NAME LIKE '%9%';
    
-- Esercizio11

SELECT FIRST_NAME, LAST_NAME, SALARY FROM ql.employees
ORDER BY SALARY DESC
LIMIT 10;

-- Query 12 JOIN

select ql.employees.FIRST_NAME, ql.employees.LAST_NAME, ql.jobs.JOB_TITLE, ql.employees.DEPARTMENT_ID from ql.employees
INNER JOIN ql.jobs ON ql.employees.JOB_ID = ql.jobs.JOB_ID
INNER JOIN ql.departments ON ql.employees.DEPARTMENT_ID = ql.departments.DEPARTMENT_ID
INNER JOIN ql.locations ON ql.departments.LOCATION_ID = ql.locations.LOCATION_ID
WHERE CITY = "London";

-- Query 13 
                        
select q1.EMPLOYEE_ID, q1.LAST_NAME, q1.MANAGER_ID, q2.LAST_NAME
from ql.employees as q1
inner join employees as q2 
on q1.MANAGER_ID = q2.EMPLOYEE_ID;

-- Query14

select distinct q2.FIRST_NAME, q2.LAST_NAME, q2.HIRE_DATE, q2.SALARY
from ql.employees as q1
inner join employees as q2 
on q1.MANAGER_ID = q2.EMPLOYEE_ID
where DATE_SUB(CURDATE(), INTERVAL 15 year) > q2.HIRE_DATE;


-- Query15

SELECT ql.departments.DEPARTMENT_NAME, ql.locations.LOCATION_ID, ql.locations.STREET_ADDRESS, ql.locations.CITY, ql.locations.STATE_PROVINCE
FROM ql.departments join ql.locations on ql.departments.LOCATION_ID = ql.locations.LOCATION_ID;

-- Query 16

select distinct ql.job_history.EMPLOYEE_ID, ql.employees.FIRST_NAME, ql.jobs.JOB_TITLE, ql.departments.DEPARTMENT_NAME, ql.job_history.START_DATE, ql.job_history.END_DATE 
from ql.job_history join ql.employees on ql.employees.EMPLOYEE_ID = ql.job_history.EMPLOYEE_ID join ql.jobs on
ql.jobs.JOB_ID = ql.employees.JOB_ID join ql.departments on
ql.departments.DEPARTMENT_ID  = ql.employees.DEPARTMENT_ID
where ql.employees.SALARY > 10000; 

-- Query aggregazione

select ql.employees.SALARY, count(ql.employees.SALARY) as numbersOfSalaries 
from ql.employees
group by ql.employees.SALARY
order by ql.employees.SALARY desc;

select max(ql.employees.SALARY) as massimo_salario, min(ql.employees.SALARY) as minimo_salario
from ql.employees;

select avg(ql.employees.SALARY) as salario_medio, count(ql.employees.EMPLOYEE_ID) as numero_impiegati
from ql.employees;

select ql.jobs.JOB_TITLE, count(ql.employees.JOB_ID) as numero_lavoratori from 
ql.employees join ql.jobs on ql.employees.JOB_ID = ql.jobs.JOB_ID
group by ql.employees.JOB_ID
order by numero_lavoratori desc;

-- Subquery1

select FIRST_NAME, LAST_NAME, SALARY from ql.employees
where SALARY > (select SALARY from ql.employees
				where LAST_NAME = "Bull");
                

-- Subquery2

select * from ql.employees;

select FIRST_NAME, LAST_NAME, DEPARTMENT_ID from ql.employees
where DEPARTMENT_ID IN (select DEPARTMENT_ID from ql.departments
						where DEPARTMENT_NAME = "IT");
                        
-- Subquery3

select FIRST_NAME, LAST_NAME from ql.employees
where MANAGER_ID <> 0
and DEPARTMENT_ID IN (select DEPARTMENT_ID from ql.departments
					  join ql.locations on ql.departments.LOCATION_ID = ql.locations.LOCATION_ID
                      where ql.locations.COUNTRY_ID = "US");

-- Subquery4

SELECT FIRST_NAME, LAST_NAME, SALARY
FROM ql.employees 
WHERE employees.SALARY = (SELECT MIN_SALARY FROM ql.jobs WHERE employees.JOB_ID = jobs.JOB_ID);
                
-- Subquery5

select FIRST_NAME, LAST_NAME, SALARY from ql.employees
where SALARY > all (select SALARY from ql.employees
				where JOB_ID = "SH_CLERK")
                order by SALARY desc;
                
-- Subquery6

select employ.EMPLOYEE_ID, employ.FIRST_NAME, employ.LAST_NAME, departments.DEPARTMENT_NAME from ql.employees as employ
join ql.departments as departments on employ.DEPARTMENT_ID = departments.DEPARTMENT_ID;

-- Subquery7

select FIRST_NAME, LAST_NAME, SALARY, JOB_ID, DEPARTMENT_ID from ql.employees as q1
where salary = (select min(SALARY) from ql.employees as q2
				where q1.DEPARTMENT_ID = q2.DEPARTMENT_ID)
                order by salary;
 
 
-- Soluzioni Simone 

SELECT first_name, last_name FROM employees 
WHERE manager_id in (select employee_id FROM employees WHERE department_id IN 
					(SELECT department_id FROM departments WHERE location_id 
                    IN (select location_id from locations where country_id='US')));


SELECT employee_id, first_name, last_name, 
(SELECT department_name FROM departments d WHERE e.department_id = d.department_id) department 
FROM employees e ORDER BY department;



select * from ql.jobs;


