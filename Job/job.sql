create database dbJob;

CREATE TABLE dbJob.job (
	job_id varchar(30) primary key,
	min_salary float,
	max_salary float
);

CREATE TABLE dbJob.reparto(
	reparto_id int primary key,
	nome varchar (30)
);

CREATE TABLE dbJob.impiegato(
	impiegato_id int primary key,
	nome varchar (30),
	job_id varchar(20),
    reparto_id int,
	foreign key (job_id) references dbjob.job(job_id),
	foreign key (reparto_id) references dbjob.reparto (reparto_id)
);

insert into dbJob.job 
values (
    "SH_DEV",
    18000,
    25000
	),
    (
    "IT_CONS",
    25000,
    45000
	),
    (
    "IT_DEV",
    19000,
    42000
	);
    
insert into dbJob.reparto 
values
		(30,
        "IT"
        );

    
insert into dbJob.impiegato 
values
    (
    118,
    "Paolo",
    "IT_DEV",
    30
	),
	(
    128,
    "Gianni",
    "SH_DEV",
    12
	);

select * from dbjob.reparto;
select * from dbjob.job;
select * from dbjob.impiegato;

update dbjob.impiegato set job_id = "IT_CONS"
where impiegato_id = 118 and reparto_id = 30 and job_id not like "SH%";

select * from dbjob.impiegato;