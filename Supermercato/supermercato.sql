CREATE DATABASE dbsupermercato;

CREATE TABLE dbsupermercato.cliente (
	codiceFiscale varchar(16) PRIMARY KEY,
    nome varchar(20),
    cognome varchar(30),
    idCartaPunti numeric(12)
    );
    
    
CREATE TABLE dbsupermercato.carrello (
	id_Carello int PRIMARY KEY AUTO_INCREMENT,
    codiceFiscale_cliente varchar(16),
    FOREIGN KEY (codiceFiscale_cliente) REFERENCES dbsupermercato.cliente(codiceFiscale)
    );
    

CREATE TABLE dbsupermercato.magazzino (
	idMagazzino int PRIMARY KEY AUTO_INCREMENT,
    indirizzo varchar(20),
    città varchar(20)
    );
    
CREATE TABLE dbsupermercato.impiegato (
	codiceFiscale varchar(16),
    id int,
    nome varchar(20),
    cognome varchar(30),
    id_magazzino int,
    FOREIGN KEY (id_magazzino) REFERENCES magazzino(idMagazzino),
    CONSTRAINT pk_impiegato PRIMARY KEY (codiceFiscale, id)
    );
    
CREATE TABLE dbsupermercato.reparto (
	id int PRIMARY KEY AUTO_INCREMENT,
    numeroProdotti int,
    nome varchar(20),
    descrizione varchar(50)
    );
    
CREATE TABLE dbsupermercato.prodotto (
	codiceABarre numeric(12) PRIMARY KEY,
    prezzo float,
    quantità int,
    tipologia varchar(20)
    );
    
CREATE TABLE dbsupermercato.contieneprodotto(
	codiceABarre_prodotto numeric(12),
    id_carrello int,
	FOREIGN KEY (codiceABarre_prodotto) REFERENCES dbsupermercato.prodotto(codiceABarre),
    FOREIGN KEY (id_carrello) REFERENCES dbsupermercato.carrello(id_Carello),
    CONSTRAINT pk_contieneprodotto PRIMARY KEY (codiceABarre_prodotto, id_carrello)
    );

CREATE TABLE dbsupermercato.compraProdotto(
	codiceFiscale_cliente varchar(16),
    codiceABarre_prodotto numeric(12),
    FOREIGN KEY (codiceFiscale_cliente) REFERENCES dbsupermercato.cliente(codiceFiscale),
    FOREIGN KEY (codiceABarre_prodotto) REFERENCES dbsupermercato.prodotto(codiceABarre),
    CONSTRAINT pk_compraProdotto PRIMARY KEY (codiceFiscale_cliente, codiceABarre_prodotto)
    );
    
    
CREATE TABLE dbsupermercato.gestisceReparto(
	codiceFiscale_impiegato varchar(16),
    id_impiegato int,
    id_reparto int,
    FOREIGN KEY (codiceFiscale_impiegato, id_impiegato) REFERENCES dbsupermercato.impiegato(codiceFiscale, id),
    FOREIGN KEY (id_reparto) REFERENCES dbsupermercato.reparto(id),
    CONSTRAINT pk_gestisceReparto PRIMARY KEY (codiceFiscale_impiegato, id_impiegato, id_reparto)
    );
    
CREATE TABLE dbsupermercato.fornitore (
	P_IVA varchar(16) PRIMARY KEY,
    nome varchar(20),
	città varchar(30),
    indirizzo varchar(40)
    );
    
    
CREATE TABLE dbsupermercato.rifornisceMagazzino (
	P_IVA_fornitore varchar(16),
	id_Magazzino int,
    FOREIGN KEY (P_IVA_fornitore) REFERENCES dbsupermercato.fornitore(P_IVA),
    FOREIGN KEY (id_Magazzino) REFERENCES dbsupermercato.magazzino(idMagazzino),
    CONSTRAINT pk_rifornisciMagazzino PRIMARY KEY (P_IVA_fornitore, id_Magazzino)
    );
    
ALTER TABLE dbsupermercato.reparto
ADD COLUMN codice varchar(8) AFTER id;

ALTER TABLE dbsupermercato.fornitore
DROP indirizzo;
    

